Comment aller plus loin ou accéder à des ressources qui nous offrent une vision complémentaire ?

[TOC]

En voici quelques unes à discuter et partager ensemble:

### Trois tutoriels sur mixité et numérique

Trois tutoriels réalisés par <a href="https://fr.wikipedia.org/wiki/Isabelle_Collet" target="_blank">Isabelle Collet</a> pour les collègues enseignant·e·s ou les personnes intervenantes en classe. Ils permettent de donner des conseils pour transmettre un numérique sans biais de genre et d’identifier les principaux écueils à éviter:

- <a href="https://tube-education.beta.education.fr/videos/watch/ea3ab613-2b7f-45e3-b012-ba714451c609" target="_blank">Animer un atelier mixte</a> parce que filles et garçons ne s’expriment pas de la même manière.
- <a href="https://tube-education.beta.education.fr/videos/watch/7bfa3d47-30f5-4b1d-9da6-858ac3cd5ad2" target="_blank">Représenter un rôle modèle</a> parce que nombreuses/nombreux rôles modèles transmettent inconsciemment des biais de genre.
- <a href="https://tube-education.beta.education.fr/videos/watch/bdf1cd9f-294c-4938-8951-2f93b9f83f8b" target="_blank">Favoriser l’intérêt des filles</a> sur des sujets pour lesquels elles viennent avec un a priori.

Ces vidéos sont libres d’usage pour servir l‘ensemble de la communauté éducative et ses partenaires. Leur utilisation peut s’imaginer, pendant et hors temps scolaire, dans le cadre de l’enseignement du numérique ou encore d’ateliers liés au numérique, de la découverte des métiers, d’interventions de rôles modèles, de médiation et de sciences du numérique.

### Mixité et numérique

Une conférence plénière d'<a href="https://fr.wikipedia.org/wiki/Isabelle_Collet" target="_blank">Isabelle Collet</a> qui y développe sa pensée relativement à la pedagogie de l'égalité. Elle est informaticienne scientifique de formation. Elle est maintenant Professeure en sciences de l’éducation à l’Université́ de Genève. Elle a publié en 2019 « <a href="https://www.franceculture.fr/oeuvre/les-oubliees-du-numerique" target="_blank">Les oubliées du numérique</a> » aux éditions Le Passeur. Elle est administratrice de la Fondation Femmes@numérique et fut Vice-présidente du Conseil d’administration de l’INSA de Lyon.

[![Video de l'intervention](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/raw/master/3_Prendre-du-recul-au-niveau-didactique/3.3_Pedagogie-de-l-egalite/Ressources/femmes-et-numerique.png)](https://scolawebtv.crdp-versailles.fr/?iframe&id=60430)

[![Diaporama de l'intervention](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/raw/master/3_Prendre-du-recul-au-niveau-didactique/3.3_Pedagogie-de-l-egalite/Ressources/femmes-et-numerique-collet.png)](https://www.dane.ac-versailles.fr/IMG/pdf/femmes-et-numerique-collet.pdf?inline=false)

Une ressource mise en partage par l'<a href="https://www.dane.ac-versailles.fr/etre-accompagne-se-former/femmes-et-numerique" target="_blank">académie de Versailles</a> .


### La boite à métiers de la filière du numérique

Une approche ludique pour prendre connaissance des métiers du numérique avec une prise en main autonome par les enseignants et une animation pour mettre en évidence et partager les risques sur les problématiques de genre dans ce secteur
- Cibles : animation en classe de 3eme et Lycée
- Projet accompagné par Académie (DRAIO) et DAN Montpellier
- Contacts : Anais Moressa anais.scienceanim@gmail.com
https://www.science-animation.org/fr/boite-metiers-numerique-femmes-et-numerique-kit-danimation 

Note : il y a un problème de certificat mais on peut passer outre les alertes du navigateur

### Le rapport ministériel sur « Faire de l’égalité filles-garçons une nouvelle étape dans la mise en œuvre du lycée du XXIe siècle » 

Ce [rapport du 9 Juillet 2021](https://www.education.gouv.fr/sites/default/files/2021-09/t-l-charger-le-rapport-faire-de-l-galit-filles-gar-ons-une-nouvelle-tape-dans-la-mise-en-oeuvre-du-lyc-e-du-xxie-si-cle--94424.pdf?inline=false) explicite un cadre réglementaire engageant et induisant de nombreuses initiatives, mais des des constats contrastés et paradoxaux en matière d’égalité filles-garçons, comme si toutes ces mesures n'étaient que peu efficaces et la solution devaint être trouvée ailleurs.

Dans l’ensemble, les filles font toujours plus le choix d’enseignements littéraires et artistiques tandis que les garçons se tournent majoritairement vers les enseignements scientifiques. En 1re, la représentation des filles sont quasi équilibrées avec
les garçons en mathématiques (de l’ordre de 50%) et sous représentées en NSI (18%) et SI (15,4%). En terminale, elles représentent 41,8% en mathématiques (soit une baisse de 9 points par rapport à la 1re), 13% en NSI (soit une baisse de 5 points) un chiffre stable mais faible en SI. La proportion des filles et des garçons dans les doublettes de terminale prolonge et accentue les inégalités des choix de spécialités en 1re. En résumé, les choix genrés persistent, la réforme du lycée mise en place sur une seule promotion n’a pas changé les inégalités de répartition, mais ne les a pas aggravées. Il existe quelques signes d’évolution que le temps permettra de vérifier. Cette asymétrie est inverse dans les matières dites "littéraires", avec peu de garçons.



### Autres projets menés avec la fondation Femmes@Numérique en collaboration avec des Académies 

La fondation <a href="https://femmes-numerique.fr" target="_blank">Femmes@Numérique</a> partage d'[autres projets](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/raw/master/3_Prendre-du-recul-au-niveau-didactique/3.3_Pedagogie-de-l-egalite/Ressources/synthese_femmes_numerique_ressources_filles_et_numerique.pdf?inline=false) pas directement liés à l'informatique au lycée, mais assez inspirants.

### Trois praticiennes parlent de leurs métiers du numérique

Les « jeudis de la recherche » de l'académie de Versailles permettent à tous les acteurs de l’éducation intéressés par un domaine de le découvrir en faisant la rencontre d’un chercheur spécialiste du domaine abordé puis en faisant celle d’un ou de plusieurs praticiens avec un témoignage concret sous l’angle des pratiques numériques.

Ici avec <a href="https://www.dane.ac-versailles.fr/etre-accompagne-se-former/article/femmes-et-numerique-2-2" target="_blank">Femmes et Numérique</a> trois praticiennes parlent de leurs métiers du numérique.

### Femmes et Sciences : et si c’était une affaire de mecs ?

Un <a href="https://www.lemonde.fr/blog/binaire/2019/12/19/femmes-et-sciences-et-si-cetait-une-affaire-de-mecs/" target="_blank">article de médiation scientifique</a> un peu décalé "écrit" par un petit garçon de 6 mois présente sur le blong Binaire du Monde.fr, à travers les contributions de deux grandes scientifiques de la pédagogie de l'égalité <a href="https://www.cnrs.fr/fr/personne/clemence-perronnet" target="_blank">Clémence Perronnet</a> et <a href="https://fr.wikipedia.org/wiki/Isabelle_Collet" target="_blank">Isabelle Collet</a> cette problématique de la pédagogie de l'égalité. Cet article collectif est issu d'une intervention au congrès de l'association <a href="https://www.femmesetsciences.fr/" target="_blank">Femmes et Sciences</a>.
