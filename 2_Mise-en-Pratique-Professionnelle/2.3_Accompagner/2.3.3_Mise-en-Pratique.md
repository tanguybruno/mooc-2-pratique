# À faire vous-même

Les objectifs de ce Module 3 sont :

1. Proposer une ou plusieurs ressources parmi :
    - une activité de remédiation pour des élèves en difficulté
    - une activité d'approfondissement d'une activité existante ; préciser s'il s'agit d'une activité pour la classe entière ou une activité supplémentaire/complémentaire proposée uniquement aux élèves en avance
    - un projet en précisant :
        - les connaissances travaillées
        - le format pédagogique (durée, seul, binôme, groupe...)
        - les étayages pour assurer une progression des élèves
        - le calendrier (points d'étapes, rendus intermédiaires...)
    - une activité pour la découverte ou la consolidation d'un concept qui autorise des cheminements différents pour atteindre un même objectif
2. Déposer vos ressources sur votre espace gitlab, ou noter le lien si les ressources sont déjà disponibles sur le web
3. Se rendre sur le Forum pour y mettre un _post_, dans la rubrique dédiée, pour annoncer le travail fait. D'autres pourront alors accéder à votre travail, le commenter, vous faire des retours
4. De votre côté, regardez ce que d'autres ont proposé, commentez, échangez avec les auteur-es.

# Liens qui pourraient vous intéresser :
-Bruno Mermet Notion de projets https://mermet.users.greyc.fr/Enseignement/EnseignementInformatiqueLycee/Havre/Didactique/projetInformatique.html

-Apprentissage collaboratif : guide pratique
https://knowledgeone.ca/apprentissage-collaboratif-guide-pratique/?lang=fr

-Nuit du code pour promouvoir NSI dès le collège - un samedi pendant 6H - gameplay déjà fait, ils doivent programmer un jeu
--> gérer par le lycée de Tokyo, se fait sur Scratch et python
