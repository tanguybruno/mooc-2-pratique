# Scénario d'un stagiaire qui arrive sur le mooc

## Module introduction

0. Il faudrait une petite vidéo qui explique le but du MOOC et son organisation dans les grandes lignes non ?
1. Le stagiaire accède à l'introduction générale https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/blob/master/0_Introduction/0_introduction.md
2. puis à l'Introduction organisationnelle https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/blob/master/0_Introduction/0_organisation.md
3. Accès aux tutos sur markdown et [gitlab](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/blob/master/0_Introduction/4_Tutoriels/2_tuto_gitlab.md) ; si j'ai bien compris on se donne un gitlab bac à sable (update il est là : https://gitlab.com/mooc-nsi-snt/mooc-2-ressources) pour que les stagiaires puissent déposer leurs ressources... on y met 4 dossiers :
    - Penser-Concevoir-Elaborer
    - Mettre-en-oeuvre-Animer
    - Accompagner
    - Observer-Analyser-Evaluer

    Plus éventuellement d'autres encore plus bac à sable pour tester l'outil gitlab

## Module Penser-Concevoir-Elaborer

4. Accès à la 1re activité du 1er niveau du 1er module : https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/blob/master/1_Penser-Concevoir-Elaborer/Niveau_1_Utiliser/Activite_01.md qui est de mon cru et que je ne trouve vraiment pas top... et que j'aimerais bien changer par une autre l'idée de cette première activité très simple : partir d'une fiche élève faite, proposer la fiche prof, et s'entrainer à la déposer sur le gitlab, et stipuler _quelque part_ ça y est j'ai posé mon travail. Est-ce qu'on utilise le forum pour ça ? une page du gitlab que les stagiaire peuvent éditer ?
5. Accès aux autres activités... l'une devra servir d'évaluation par les pairs

## etc. les autres modules _pratique_


## Module _Aller plus loin_ 
_il faut trouver un titre pour pouvoir mettre ici les compléments sur la didactique et la sensibilisation au problème du genre_

6. Introduction https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/blob/master/Prendre%20du%20recul%20au%20niveau%20didactique/introduction.md
7. Et aujourd'hui ? A-t-on des exemples de questionnements de didactique ? : la vidéo d'Olivier
8. Le problème de genre... pistes de réflexions pour en prendre conscience et tenter de le combattre : les vidéos sur le genre

...

## Module Se préparer aux concours