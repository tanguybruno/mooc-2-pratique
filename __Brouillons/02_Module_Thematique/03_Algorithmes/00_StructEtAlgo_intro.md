# Fiche thématique

## Structures et algorithmes

Cette rubrique est à cheval sur deux des thèmes du [programme officiel de Terminale](https://cache.media.education.gouv.fr/file/SPE8_MENJ_25_7_2019/93/3/spe247_annexe_1158933.pdf) :

### Structures de données

| Contenus | Capacités attendues | Commentaires |
| -------- | ------------------- | ------------ |
| Listes, piles, files :<br>structures linéaires. Dictionnaires, index et clé. | Distinguer des structures par le jeu des méthodes qui les caractérisent.<br>Choisir une structure de données adaptée à la situation à modéliser.<br> Distinguer la recherche d’une valeur dans une liste et dans un dictionnaire. | On distingue les modes FIFO (first in first out) et LIFO (last in first out) des piles et des files.
| Arbres : structures hiérarchiques. <br>Arbres binaires : nœuds, racines,feuilles, sous-arbres gauches, sous-arbres droits. |  Identifier des situations nécessitant une structure de données arborescente.<br>Évaluer quelques mesures des arbres binaires (taille, encadrement de la hauteur, etc.). | On fait le lien avec la rubrique algorithmique.
| Graphes : structures relationnelles. <br>Sommets, arcs, arêtes, graphes orientés ou non orientés. | Modéliser des situations sous forme de graphes. <br>Écrire les implémentations correspondantes d’un graphe : matrice d’adjacence, liste de successeurs/de prédécesseurs. Passer d’une représentation à une autre. | On s’appuie sur des exemples comme le réseau routier, le réseau électrique, <br>Internet, les réseaux sociaux. Le choix de la représentation dépend du traitement qu’on veut mettre en place : <br>on fait le lien avec la rubrique algorithmique. |


### Algorithmes

| Contenus | Capacités attendues | Commentaires |
| -------- | ------------------- | ------------ |
| Algorithmes sur les arbres binaires <br>et sur les arbres binaires de recherche. | Calculer la taille et la hauteur d’un arbre.<br> Parcourir un arbre de différentes façons (ordres infixe, préfixe ou suffixe ; ordre en largeur d’abord).<br> Rechercher une clé dans un arbre de recherche, insérer une clé. | Une structure de données récursive adaptée est utilisée.<br>L’exemple des arbres permet d’illustrer la programmation par classe.<br>La recherche dans un arbre de recherche équilibré est de coût logarithmique.
| Algorithmes sur les graphes.| Parcourir un graphe en profondeur d’abord, en largeur d’abord.<br> Repérer la présence d’un cycle dans un graphe.<br> Chercher un chemin dans un graphe.|Le parcours d’un labyrinthe et le routage dans Internet sont des exemples d’algorithme sur les graphes.<br> L’exemple des graphes permet d’illustrer l’utilisation des classes en programmation.